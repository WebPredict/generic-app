class ApplicationMailer < ActionMailer::Base
  default from: "admin@fast-everglades-2137.herokuapp.com"
  layout 'mailer'
end
